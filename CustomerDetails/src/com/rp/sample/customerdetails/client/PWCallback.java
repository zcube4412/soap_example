package com.rp.sample.customerdetails.client;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

/**
 * Client side handler for setting up the user password for a given user wss4j.
 * This component can have the integration with the LDAp or the DB for getting
 * and setting the user credential in plain format
 * 
 * @author rizwan
 *
 */
public class PWCallback implements CallbackHandler {

	@Override
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		for (Callback callback : callbacks) {
			if (callback instanceof WSPasswordCallback) {
				WSPasswordCallback wspcallback = (WSPasswordCallback) callback;
				if ("wss4j".equals(wspcallback.getIdentifier())) {
					wspcallback.setPassword("security");
				}
			} else {
				throw new UnsupportedCallbackException(callback,
						"Unrecognized callback**");
			}
		}
	}

}
