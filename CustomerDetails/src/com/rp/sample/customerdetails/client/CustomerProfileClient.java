package com.rp.sample.customerdetails.client;

import java.rmi.Remote;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.EngineConfiguration;
import org.apache.axis.client.Stub;
import org.apache.axis.configuration.FileProvider;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.apache.ws.security.message.token.UsernameToken;

import com.rp.sample.customerdetails.CustomerDetails_PortType;
import com.rp.sample.customerdetails.CustomerDetails_Service;
import com.rp.sample.customerdetails.CustomerDetails_ServiceLocator;
import com.rp.sample.customerdetails.CustomerProfileRequestType;
import com.rp.sample.customerdetails.CustomerProfileResponseType;

public class CustomerProfileClient {

	public static void main(String[] args) throws ServiceException,
			RemoteException {
		// One way to configure the client username token via client config
		EngineConfiguration config = new FileProvider("client_deploy.wsdd");
		CustomerDetails_ServiceLocator servicelocator = new CustomerDetails_ServiceLocator(
				config);
		CustomerDetails_PortType service = servicelocator
				.getCustomerDetailsSOAP();

		CustomerProfileRequestType customerProfileRequest = new CustomerProfileRequestType();

		customerProfileRequest.setProfileName("Patel");
		CustomerProfileResponseType response = service
				.customerProfile(customerProfileRequest);
		System.out.println("Response ----> " + response.toString());
	}
}
