package com.rp.sample.customerdetails.wss;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
/**
 * Server side Callback handler responsible for authenticating the user 
 * @author rizwan
 *
 */
public class PWCallback implements CallbackHandler {

	@Override
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		for(Callback callback:callbacks){
			if(callback instanceof WSPasswordCallback){
				WSPasswordCallback wspcallback = (WSPasswordCallback)callback;
				if("wss4j".equals(wspcallback.getIdentifier())){
					wspcallback.setPassword("security");
				}
			}else{
				throw new UnsupportedCallbackException(callback,"Unrecognized callback**");
			}
		}
	}

}
