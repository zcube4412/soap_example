/**
 * CustomerDetailsSOAPSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.rp.sample.customerdetails;

public class CustomerDetailsSOAPSkeleton implements com.rp.sample.customerdetails.CustomerDetails_PortType, org.apache.axis.wsdl.Skeleton {
    private com.rp.sample.customerdetails.CustomerDetails_PortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://sample.rp.com/customerdetails/", "customerProfileRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://sample.rp.com/customerdetails/", "customerProfileRequestType"), com.rp.sample.customerdetails.CustomerProfileRequestType.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("customerProfile", _params, new javax.xml.namespace.QName("http://sample.rp.com/customerdetails/", "customerProfileResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://sample.rp.com/customerdetails/", "customerProfileResponseType"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "customerProfile"));
        _oper.setSoapAction("http://sample.rp.com/customerdetails/customerProfile");
        _myOperationsList.add(_oper);
        if (_myOperations.get("customerProfile") == null) {
            _myOperations.put("customerProfile", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("customerProfile")).add(_oper);
    }

    public CustomerDetailsSOAPSkeleton() {
        this.impl = new com.rp.sample.customerdetails.CustomerDetailsSOAPImpl();
    }

    public CustomerDetailsSOAPSkeleton(com.rp.sample.customerdetails.CustomerDetails_PortType impl) {
        this.impl = impl;
    }
    public com.rp.sample.customerdetails.CustomerProfileResponseType customerProfile(com.rp.sample.customerdetails.CustomerProfileRequestType customerProfileRequest) throws java.rmi.RemoteException
    {
        com.rp.sample.customerdetails.CustomerProfileResponseType ret = impl.customerProfile(customerProfileRequest);
        return ret;
    }

}
