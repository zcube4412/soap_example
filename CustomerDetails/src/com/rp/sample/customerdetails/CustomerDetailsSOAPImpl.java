/**
 * CustomerDetailsSOAPImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.rp.sample.customerdetails;

import java.util.Date;

public class CustomerDetailsSOAPImpl implements com.rp.sample.customerdetails.CustomerDetails_PortType{
    public com.rp.sample.customerdetails.CustomerProfileResponseType customerProfile(com.rp.sample.customerdetails.CustomerProfileRequestType customerProfileRequest) throws java.rmi.RemoteException {
        
    	CustomerProfileResponseType response = new CustomerProfileResponseType();
    	response.setProfileName("Dummy Profile");
    	response.setProfileSecretNumber(1234);
    	NomineeType profileNominee = new NomineeType();
    	profileNominee.setFirstName("Rizwan");
    	profileNominee.setLastName("Patel");
    	profileNominee.setDateOfBirth(new Date());
    	profileNominee.setId("Secret1");
		response.setProfileNominee(profileNominee );
    	return response;
    }

}
