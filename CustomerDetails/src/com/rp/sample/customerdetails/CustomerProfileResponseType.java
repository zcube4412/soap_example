/**
 * CustomerProfileResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.rp.sample.customerdetails;

public class CustomerProfileResponseType  implements java.io.Serializable {
    private java.lang.String profileName;

    private int profileSecretNumber;

    private com.rp.sample.customerdetails.NomineeType profileNominee;

    public CustomerProfileResponseType() {
    }

    public CustomerProfileResponseType(
           java.lang.String profileName,
           int profileSecretNumber,
           com.rp.sample.customerdetails.NomineeType profileNominee) {
           this.profileName = profileName;
           this.profileSecretNumber = profileSecretNumber;
           this.profileNominee = profileNominee;
    }


    @Override
	public String toString() {
		return "CustomerProfileResponseType [profileName=" + profileName
				+ ", profileSecretNumber=" + profileSecretNumber
				+ ", profileNominee=" + profileNominee + ", __equalsCalc="
				+ __equalsCalc + ", __hashCodeCalc=" + __hashCodeCalc + "]";
	}

	/**
     * Gets the profileName value for this CustomerProfileResponseType.
     * 
     * @return profileName
     */
    public java.lang.String getProfileName() {
        return profileName;
    }


    /**
     * Sets the profileName value for this CustomerProfileResponseType.
     * 
     * @param profileName
     */
    public void setProfileName(java.lang.String profileName) {
        this.profileName = profileName;
    }


    /**
     * Gets the profileSecretNumber value for this CustomerProfileResponseType.
     * 
     * @return profileSecretNumber
     */
    public int getProfileSecretNumber() {
        return profileSecretNumber;
    }


    /**
     * Sets the profileSecretNumber value for this CustomerProfileResponseType.
     * 
     * @param profileSecretNumber
     */
    public void setProfileSecretNumber(int profileSecretNumber) {
        this.profileSecretNumber = profileSecretNumber;
    }


    /**
     * Gets the profileNominee value for this CustomerProfileResponseType.
     * 
     * @return profileNominee
     */
    public com.rp.sample.customerdetails.NomineeType getProfileNominee() {
        return profileNominee;
    }


    /**
     * Sets the profileNominee value for this CustomerProfileResponseType.
     * 
     * @param profileNominee
     */
    public void setProfileNominee(com.rp.sample.customerdetails.NomineeType profileNominee) {
        this.profileNominee = profileNominee;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerProfileResponseType)) return false;
        CustomerProfileResponseType other = (CustomerProfileResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.profileName==null && other.getProfileName()==null) || 
             (this.profileName!=null &&
              this.profileName.equals(other.getProfileName()))) &&
            this.profileSecretNumber == other.getProfileSecretNumber() &&
            ((this.profileNominee==null && other.getProfileNominee()==null) || 
             (this.profileNominee!=null &&
              this.profileNominee.equals(other.getProfileNominee())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProfileName() != null) {
            _hashCode += getProfileName().hashCode();
        }
        _hashCode += getProfileSecretNumber();
        if (getProfileNominee() != null) {
            _hashCode += getProfileNominee().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerProfileResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://sample.rp.com/customerdetails/", "customerProfileResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "profileName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileSecretNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "profileSecretNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileNominee");
        elemField.setXmlName(new javax.xml.namespace.QName("", "profileNominee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://sample.rp.com/customerdetails/", "nomineeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
