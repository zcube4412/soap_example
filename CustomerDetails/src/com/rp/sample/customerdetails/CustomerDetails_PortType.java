/**
 * CustomerDetails_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.rp.sample.customerdetails;

public interface CustomerDetails_PortType extends java.rmi.Remote {
    public com.rp.sample.customerdetails.CustomerProfileResponseType customerProfile(com.rp.sample.customerdetails.CustomerProfileRequestType customerProfileRequest) throws java.rmi.RemoteException;
}
