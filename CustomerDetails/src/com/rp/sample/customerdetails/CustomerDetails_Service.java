/**
 * CustomerDetails_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.rp.sample.customerdetails;

public interface CustomerDetails_Service extends javax.xml.rpc.Service {
    public java.lang.String getCustomerDetailsSOAPAddress();

    public com.rp.sample.customerdetails.CustomerDetails_PortType getCustomerDetailsSOAP() throws javax.xml.rpc.ServiceException;

    public com.rp.sample.customerdetails.CustomerDetails_PortType getCustomerDetailsSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
