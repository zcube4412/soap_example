/**
 * CustomerDetails_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.rp.sample.customerdetails;

public class CustomerDetails_ServiceLocator extends org.apache.axis.client.Service implements com.rp.sample.customerdetails.CustomerDetails_Service {

    public CustomerDetails_ServiceLocator() {
    }


    public CustomerDetails_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CustomerDetails_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CustomerDetailsSOAP
    private java.lang.String CustomerDetailsSOAP_address = "http://localhost:8080/CustomerDetails/services/CustomerProfile";

    public java.lang.String getCustomerDetailsSOAPAddress() {
        return CustomerDetailsSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CustomerDetailsSOAPWSDDServiceName = "CustomerDetailsSOAP";

    public java.lang.String getCustomerDetailsSOAPWSDDServiceName() {
        return CustomerDetailsSOAPWSDDServiceName;
    }

    public void setCustomerDetailsSOAPWSDDServiceName(java.lang.String name) {
        CustomerDetailsSOAPWSDDServiceName = name;
    }

    public com.rp.sample.customerdetails.CustomerDetails_PortType getCustomerDetailsSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CustomerDetailsSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCustomerDetailsSOAP(endpoint);
    }

    public com.rp.sample.customerdetails.CustomerDetails_PortType getCustomerDetailsSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.rp.sample.customerdetails.CustomerDetailsSOAPStub _stub = new com.rp.sample.customerdetails.CustomerDetailsSOAPStub(portAddress, this);
            _stub.setPortName(getCustomerDetailsSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCustomerDetailsSOAPEndpointAddress(java.lang.String address) {
        CustomerDetailsSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.rp.sample.customerdetails.CustomerDetails_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.rp.sample.customerdetails.CustomerDetailsSOAPStub _stub = new com.rp.sample.customerdetails.CustomerDetailsSOAPStub(new java.net.URL(CustomerDetailsSOAP_address), this);
                _stub.setPortName(getCustomerDetailsSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CustomerDetailsSOAP".equals(inputPortName)) {
            return getCustomerDetailsSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://sample.rp.com/customerdetails/", "CustomerDetails");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://sample.rp.com/customerdetails/", "CustomerDetailsSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CustomerDetailsSOAP".equals(portName)) {
            setCustomerDetailsSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
